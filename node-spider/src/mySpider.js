const express = require('express')
const app = express()
const superagent = require('superagent')
const cherrio = require('cheerio')
// const fs = require('fs')

let content = []

superagent.get('https://cnodejs.org/').end((err, res) => {
    if(err){
        console.log(`内容爬取成功  - ${err}`);
    }
    console.log('内容爬取成功');
    content = getContent(res)
})

let getContent = (res) => {
    let content = []
    let $ = cherrio.load(res.text)
    // 图片
    $('div#topic_list div a img').each((idx, ele) => {
        let img_url = {
            src: $(ele).attr('src')
        }
        content.push(img_url)
    })
    // 文章地址
    $('div#topic_list div div a').each((idx, ele) => {
        let img_url = {
            href: $(ele).attr('href'),
            title: $(ele).attr('title')
        }
        content.push(img_url)
    })
    return content
} 

app.get('/', function (req, res) {
    res.send('hello world') 
})

app.get('/', function (req, res) {
    res.send() 
})

app.get('/getcontent', async (req, res, next) => {
    res.send(content)
})

app.listen(3000, () => console.log('the app is run at :localhost:3000'))