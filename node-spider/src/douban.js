const express = require('express');
const app = express();
const fs = require('fs')
// 引入所需要的第三方包
const superagent = require('superagent');
const cheerio = require('cheerio');

let data = [] // 存放数据
let page = []

superagent.get('https://movie.douban.com/top250').end((err, res) => {
    if(err) {
        console.log(`获取数据失败 - ${err}`)
    }
    console.log('数据获取成功')
    data = getData(res)
    page = res
})

let getData = (res) => {
    let _data = []
    let $ = cheerio.load(res.text)
    $('li .item .pic img').each((idx, ele) => {
        let content = {
            pic: $(ele).attr('src'),
            // href: $('.info .hd a').attr('href'),
            // title: $('.title').text(),
            // // person: $('.info .bd p').text(),
            // star:$('.rating_num').text(),
            // quote: $('.quote').text(),
        }
        _data.push(content)
    })
    console.log('this---------', _data);
    return _data
}

app.get('/', (req, res) => {
    res.send(data)
})
app.get('/page', (req, res) => {
    res.send(page)
})

app.listen(3000, () => console.log('run in : localhost:3000'))