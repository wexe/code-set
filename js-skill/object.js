// let book = {}; 
// Object.defineProperties(book, {
//     year_: { value: 2017 }, 
//     edition: { value: 1 }, 
//     year: { 
//         get: function () { return this.year_; }, 
//         set: function (newValue) { 
//             if (newValue > 2017) { 
//                 this.year_ = newValue; 
//                 this.edition += newValue - 2017;
//             }
//         }
//     }
//  });

//  console.log(Object.getOwnPropertyDescriptors(book)); 

let book = {};
Object.defineProperties(book, {
    year_: {
        value: 2017
    },
    edition: {
        value: 1
    },
    year: {
        get: function () {
            return this.year_;
        },
        set: function (newValue) {
            if (newValue > 2017) {
                this.year_ = newValue;
                this.edition += newValue - 2017;
            }
        }
    }
});
let descriptor = Object.getOwnPropertyDescriptor(book, "year_");
console.log(descriptor.value); // 2017 
console.log(descriptor.configurable); // false 
console.log(typeof descriptor.get); // "undefined" 
let descriptor_ = Object.getOwnPropertyDescriptor(book, "year");
console.log(descriptor_.value); // undefined 
console.log(descriptor_.enumerable); // false 
console.log(typeof descriptor_.get); // "function"