function okk(str,r) {
    return (r) => {
        return (r.value.toLowerCase().indexOf(str.toLowerCase()) === 0)
    }
}

let str = 'abced'

// console.log(okk(str))

function callbackFunc(a,b) {
    let c = a + b
    console.log(c);
    return (a,b) => {
        return a + b
    }
}
console.log(callbackFunc(10,39));