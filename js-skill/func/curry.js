function add (x, y) { 
    return x + y
}

let curry = function (x) {
    return function (y) {
        return x + y
    }
}

console.log('add:',add(12,34),'curry:', curry(10)(20));