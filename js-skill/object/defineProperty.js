let human = {name:'zhangsang', sex:'男'}
Object.defineProperty(human, 'age' , {value:20, writable:true,configurable:true,enumerable:true})
human.age = 19
// console.log(human);

let Object1 = {}
Object.defineProperty(Object1, 'property1', {
  value: 12,
  writable: false
})
Object1.property1 = 90
console.log(Object1.property1);

// const object1 = {};

// Object.defineProperty(object1, 'property1', {
//   value: 42,
//   writable: false
// });

// object1.property1 = 77;
// // throws an error in strict mode

// console.log(object1.property1);
// expected output: 42
