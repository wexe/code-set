function _new () {
  let target = {}
  let [constructor, ...args] = [...arguments]
  target.__proto__ = constructor.prototype
  let result = constructor.apply(target, args)
  if(result && ( typeof  result == 'object' || typeof result == 'function')) {
    return result
  }
  return target
}

let B = {a:1,b:2}

console.log(_new());