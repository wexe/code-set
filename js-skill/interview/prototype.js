// console.log(Function.prototype == Object.__proto__)

function test() {
  let a = 1;
  console.log(a);
}


function Animal(a){
  this.a = a
  console.log(a);
  let obj = {
    a:'obj',
    b:'BBB'
  }
  console.log(obj.a);
}

let dog = new Animal(123)
console.log(dog.__proto__);
