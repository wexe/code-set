const target = {
    foo: 'bar',
    sex: 'nan',
    name: 'wujihan'
}

const handle = {
    // get() { 
    //     return Reflect.get(...arguments);
    // }
    get: Reflect.get
}

const p = new Proxy(target, handle)

console.log(p.name, target.name)


const proxy = new Proxy(target, Reflect); 
console.log(proxy.foo); // bar 
console.log(target.foo); // bar