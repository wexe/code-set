
// 重构前
// const o = {};
// try {
//     Object.defineProperty(o, 'foo', 'bar');
//     console.log('success');
// } catch (e) {
//     console.log('failure');
// }

// 重构后
const o = {};
if (Reflect.defineProperty(o, 'foo', { value: 'bar' })) {
    console.log('success');
} else {
    console.log('failure');
}