const target = {
    neme: 'wjh'
}
const handle = {}

const p = new Proxy(target, handle)

console.log(p);

target.id = 'target'
console.log(target.id);
console.log(p.id);

p.id = 'bat'
console.log('p.id:', p.id, 'target.id:', target.id);
console.log(p.hasOwnProperty('id'), target.hasOwnProperty('id'));

