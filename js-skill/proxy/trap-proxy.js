const target = {
    name: 'bar',
    sex: 'nan'
}

const handle = {
    get(trapTarget, property, receiver) { 
        // console.log(trapTarget === target); 
        // console.log(property); 
        // console.log(receiver === p); 


        return trapTarget[property];
        
    }
}

const p = new Proxy(target, handle)
// console.log('p.name：', p.name, 'target.name:', target.name);
// console.log('p[name]：', p['name'], 'target[name]:', target['neme']);
// console.log(Object.create(target)['name']); // bar 
// console.log(Object.create(p)['name']); // handler override

console.log(p.name, target.name)
