class ListNode {
    constructor(key) {
        this.next = null;
        this.key = key;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.length = 0;
    }

    static createNode(key) {
        return new ListNode(key);
    }

    // 往头部插入数据
    insert(node) {
        // 如果head后面有指向的节点
        if (this.head) {
            node.next = this.head;
        } else {
            node.next = null;
        }
        this.head = node;
        this.length++;
    }

    find(key) {
        let node = this.head;
        while (node !== null && node.key !== key) {
            node = node.next;
        }
        return node;
    }

    delete(node) {
        if (this.length === 0) {
            throw 'node is undefined';
        }

        if (node === this.head) {
            this.head = node.next;
            this.length--;
            return;
        }

        let prevNode = this.head;

        while (prevNode.next !== node) {
            prevNode = prevNode.next;
        }

        if (node.next === null) {
            prevNode.next = null;
        }
        if (node.next) {
            prevNode.next = node.next;
        }
        this.length--;
    }
}

let list = new LinkedList()

list.insert(10)
list.insert(20)
list.insert(30)
console.log(list);