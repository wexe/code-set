let Node = function (element) {
    this.element = element;
    this.next = null;
}


class LinkedList {
    constructor() {
        this.length = 0;
        this.head = null;
    }

    // 返回链表中索引所对应的元素
    getElementAt (position) {
        if (position < 0 || position >= this.length) return null
        let current = this.head
        for(let i = 0; i < position; i++) {
            current = current.next
        }
        return current
    }

    // 向链表中添加节点
    append (element) {
        const node = new Node(element)
        if(this.head == null) {
            this.head = node
        } else {
            let current = this.getElementAt(this.head.length - 1)
            current.next = node
        }
        this.length ++
    }

    // 在链表的指定位置插入节点    
    insert (position, element) {
        if(position < 0 || position > this.length) return false
        
        const node = new Node(element)
        if(position === 0) {
            node.next = this.head
            this.head = node
        } else {
            let previous = this.getElementAt(position - 1)
            node.next = previous.next
            previous.next = node
        }
        this.length ++
        return true
    }

    // 删除链表中指定位置的元素，并返回这个元素的值
    removeAt (position) {
        if (position < 0 || position > this.length -1) return null
        const current = this.head
        if(position == 0) {
            this.head = current.next
        } else {
            let previous = this.getElementAt(position - 1)
            current.next = previous.next
            previous.next = current
        }
        this.length --
        return current.element
    }

    // 删除链表中对应的元素
    remove (element) {
        let index = this.indexOf(element)
        return this.removeAt(index)
    }

    // 在链表中查找给定元素的索引
    indexOf (element) {
        let current = this.head
        for(let i = 0; i < this.length; i++) {
            if(current.element === element) return i
            current = current.next
        }
        return -1
    }

    // 判断链表是否为空
    isEmpty () {

        // if(this.length > 0) return false
        // return true

        // return this.head === null

        return this.length === 0

    }

    // 返回链表的长度
    size () {
        return this.length
    }

    // 返回链表的头元素
    getHead () {
        return this.head
    }
    
    // 清空链表
    clear () {
        this.head = null
        this.length = 0
    }
 
    // 辅助方法，按指定格式输出链表中的所有元素，方便测试验证结果
    toString () {
        let current = this.head
        let s = ''
        while (current) {
            let next = current.next
            next = next ? current.next : 'null'
            s += `[element: ${current.element}, next: ${next}]`
            current = current.next
        }
        return s
    }
}

let linkedList = new LinkedList();
linkedList.append(10);
linkedList.append(15);
linkedList.append(20);

console.log(linkedList.toString());

linkedList.insert(0, 9);
linkedList.insert(2, 11);
linkedList.insert(3, 25);
console.log(linkedList.toString());

console.log(linkedList.removeAt(0));
console.log(linkedList.removeAt(1));
console.log(linkedList.removeAt(3));
console.log(linkedList.toString());

console.log(linkedList.indexOf(20));

linkedList.remove(20);

console.log(linkedList.toString());

linkedList.clear();
console.log(linkedList.size());

