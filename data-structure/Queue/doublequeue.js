// Queue类
class doubleQueue {
    constructor() {
        this.items = []
    }

    // 向队列尾部添加元素
    enqueue(element) {
        this.items.push(element)
    }
    addFront(element) {
        if (this.isEmpty()) {// {1}  
            this.addBack(element);
        } else if (this.lowestCount > 0) { // {2}  
            this.lowestCount--; this.items[this.lowestCount] = element;
        } else {
            for (let i = this.count; i > 0; i--) { // {3}  
                this.items[i] = this.items[i - 1];
            }
            this.count++;
            this.lowestCount = 0;
            this.items[0] = element;// {4} 
        }
    }
    // 移除队列的第一个元素，并返回被移除的元素
    dequeue() {
        return this.items.shift()
    }

    // 返回队列的第一个元素
    front() {
        return this.items[0]
    }

    // 判断是否为空队列
    isEmpty() {
        return this.items.length === 0
    }

    // 获取队列的长度
    size() {
        return this.items.length
    }

    // 清空队列
    clear() {
        this.items = []
    }

    // 打印队列里的元素
    print() {
        console.log(this.items.toString())
    }
}

