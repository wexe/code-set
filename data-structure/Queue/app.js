// Queue类
class Queue {
    constructor() {
        this.items = []
    }

    // 向队列尾部添加元素
    enqueue(element) {
        this.items.push(element)
    }

    // 移除队列的第一个元素，并返回被移除的元素
    dequeue() {
        return this.items.shift()
    }

    // 返回队列的第一个元素
    front() {
        return this.items[0]
    }

    // 判断是否为空队列
    isEmpty() {
        return this.items.length === 0
    }

    // 获取队列的长度
    size() {
        return this.items.length
    }

    // 清空队列
    clear() {
        this.items = []
    }

    // 打印队列里的元素
    print() {
        console.log(this.items.toString())
    }
}




function generatePrintBinary(n) {
    let q = new Queue()
    q.enqueue('1')
    while (n-- > 0) {
      let s1 = q.front()
      q.dequeue()
    //   console.log(s1)
      let s2 = s1
      q.enqueue(s1 + '0')
      q.enqueue(s2 + '1')
    }
    return q.front()
  }
  
  generatePrintBinary(10) // => 1 10 11 100 101


// // 实现击鼓传花
// function hotPotato (nameList, num) {
//     let queue = new Queue();
//     // 参与游戏的选手入队
//     for (let i = 0; i < nameList.length; i++) {
//       queue.enqueue(nameList[i]);
//     }
//     // 记录被淘汰的参与者
//     let eliminated = '';
//     let n = Math.floor(Math.random()*num + 1)
//     while (queue.size() > 1) {
//       // 循环 1 到 num 次，队首出来去到队尾
//       for (let i = 0; i < n; i++) {
//         queue.enqueue(queue.dequeue());
//       }
//       // 循环1 到 num 次过后，移除当前队首的元素
//       eliminated = queue.dequeue();
//       console.log(`${eliminated} 在击鼓传花中被淘汰！`);
//     }
  
//     // 最后只剩一个元素
//     return queue.dequeue();
//   }
  
//   // 测试
//   let nameList = ["孙悟空", "唐僧", "沙僧", "白龙马", "猪八戒",'猪八戒他二姨'];
//   let winner = hotPotato(nameList, 10);
//   console.log(`最后的胜利者是：${winner}`);



// // 创建Queue实例
// let queue = new Queue();
// console.log(queue.isEmpty()); // true
// queue.enqueue('John'); // undefined
// queue.enqueue('Jack'); // undefined
// queue.enqueue('Camila'); // undefined
// queue.print(); // "John,Jack,Camila"
// console.log(queue.size()); // 3
// console.log(queue.isEmpty()); // false
// queue.dequeue(); // "John"
// queue.dequeue(); // "Jack"
// queue.print(); // "Camila"
// queue.clear(); // undefined
// console.log(queue.size()); // 0


// // BFS 
// /**
//  * @param {number} n
//  * @return {number}
//  */
//  let numSquares = function(n) {
//     let level = 0
//     let queue = new Queue()
//     while(!queue.isEmpty()) {
//         let size = queue.size()
//         level ++
//         for(let i = 0; i < size; i++) {

//         }
//     }

//     return level
// };
