class Deque {
    constructor() {
        this.items = {} // {1} 用一个对象来存储元素
        this.count = 0  // {2} 控制队列的大小
        this.lowestCount = 0 // {3} 用来追踪（头部）第一个元素
    }
    // 在队列头添加元素
    addFront(ele) {
        if (this.isEmpty()) {  // {1} 第一种场景是这个双端队列是空的
            this.items[this.count] = ele
        } else if (this.lowestCount > 0) { // {2} 第二种场景是一个元素已经被从双端队列的前端移除
            this.lowestCount -= 1
            this.items[this.lowestCount] = ele
        } else {
            for (let i = this.count; i > 0; i--) {  // {3} 将所有元素后移一位
                this.items[i] = this.items[i - 1]
            }
            this.items[0] = ele // {4} 用需要添加的新元素来覆盖它
        }
        this.count++
        return ele
    }
    // 移除队列头的元素
    removeFront() {
        if (this.isEmpty()) {
            return
        }
        const delEle = this.items[this.lowestCount]
        delete this.items[this.lowestCount]  // delete为JavaScript 删除一个对象的关键字api
        this.lowestCount++
        return delEle
    }
    // 在队列尾部添加元素
    addBack(ele) {
        this.items[this.count] = ele
        this.count++
    }
    // 移除队列尾部的元素
    removeBack() {
        if (this.isEmpty()) {
            return
        }

        const delEle = this.items[this.count - 1]
        delete this.items[this.count - 1]
        this.count--
        return delEle
    }
    // 获取双端队列头部的第一个元素
    peekFront() {
        if (this.isEmpty()) {
            return
        }
        return this.items[this.lowestCount]
    }
    // 获取双端队列尾部的第一个元素
    peekBack() {
        if (this.isEmpty()) {
            return
        }
        return this.items[this.count - 1]
    }

    size() {
        return this.count - this.lowestCount
    }

    isEmpty() {
        return this.size() === 0
    }

    clear() {
        this.items = {}
        this.count = 0
        this.lowestCount = 0
    }

    toString() {
        if (this.isEmpty()) {
            return ''
        }
        let objString = `${this.items[this.lowestCount]}`
        for (let i = this.lowestCount + 1; i < this.count; i++) {
            objString = `${objString}, ${this.items[i]}`
        }
        return objString
    }

}

function palindromeChecker(str) {

    // 判断输入条件是否为空、是否为字符串类型
    if (!str || typeof str !== 'string' || !str.trim().length) {
      return false
    }
    const deque = new Deque()

    // 转化为小写
    const lowerString = str.toLowerCase().split(' ').join('')

    // 每个字符加入队列
    for (let i = 0; i < lowerString.length; i++) {
      deque.addBack(lowerString[i])
    }
  
    let isEqual = true
    let firstChar = ''
    let lastChar = ''
  
    while (deque.size() > 1 && isEqual) {
      firstChar = deque.removeFront()
      lastChar = deque.removeBack()

    //   两端一起往中间推，依次判断是否回文
      if (firstChar != lastChar) {
        isEqual = false
      }
    }
    return isEqual
  }

  let a = 'ABXBA'
  let b = 'fddf'
  let c = 'abcdefg'
  console.log(palindromeChecker(a)) // true 当前为回文
  console.log(palindromeChecker(b)) // true 当前为回文
  console.log(palindromeChecker(c)) // false 当前不为回文
  