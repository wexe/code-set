// "use strict";
// var foo = 123;
// function print(){
//     console.log('print this is ', this); 
//     // console.log(window.foo)
//     // console.log(this.foo);
// }
// console.log('global this is ', this);
// console.log(window);
// print();

a = 1;
function foo() {
    console.log(this.a); 
}
const obj = {
    a: 10,
    bar() {
        foo(); // 1
    }
}
obj.bar(); 