const data = [
    {
      baggage: 'F-2PC',
      changeCost: null,
      currencyType: 'CNY',
      otaxInfos: [],
      priceLevel: 'YOWKR1',
      roe: '1.0',
      taxInfos: [
        { taxIndex: null, taxCode: 'CN', belong: null, amount: 90, currency: 'CNY' },
        { taxIndex: null, taxCode: 'CN', belong: null, amount: 90, currency: 'CNY' }
      ],
      ticketPrice: 2670,
      totalPrice: 3147,
      travellerType: 0,
      travellerTypeName: '成人',
      upgradeCost: 2670
    },
    {
      baggage: 'F-2PC',
      changeCost: null,
      currencyType: 'CNY',
      otaxInfos: [],
      priceLevel: 'YOWKR1',
      roe: '1.0',
      taxInfos: [
        { taxIndex: null, taxCode: 'CN', belong: null, amount: 90, currency: 'CNY' },
        { taxIndex: null, taxCode: 'CN', belong: null, amount: 90, currency: 'CNY' }
      ],
      ticketPrice: 2670,
      totalPrice: 2947,
      travellerType: 0,
      travellerTypeName: '成人',
      upgradeCost: 2670
    }
  ]
  
//   Object.keys(data[0]).forEach(key => {
//       console.log(data[0][key]);
//   })
// console.log(Object.keys(data[0]))
function getFormData(object) {
    const formData = new FormData()
    Object.keys(object).forEach(key => {
        const value = object[key]
        if (Array.isArray(value)) {
            value.forEach((subValue, i) =>
                formData.append(key + `[${i}]`, subValue)
            )
        } else {
            formData.append(key, object[key])
        }
    })
    return formData
}
console.log(getFormData(data[0]));